# Static Code Analyzer

## Requirements
Implement a static code analyzer for java:
- "equals(Object obj)" and "hashCode()" should be overridden in pairs. In order to comply with the Java Language 
Specification, those methods should be either both inherited, or both overridden.
- Check for public class variable fields. Class variable fields should not have public accessibility as they do not 
respect the encapsulation principle and the behavior of the program may be unpredictable. By using private attributes 
and accessor methods (set and get), unauthorized modifications are prevented.
- Check for unused imports
- Check for to many nested loops

## Implementation
To implement the static code analyzer I chose the following approach
 - Tokenize the code string
 - Build the AST
 - Analyze the AST
For the first two steps I chose the use the [JavaParser](https://github.com/javaparser/javaparser) library.
To analyze the AST for different checks I use an AST visitor from the JavaParser library.
To avoid the duplication code required for each type of check I have a base checker that extends the JavaParser AST 
visitor where, for each file to be analyzed, I read the file and build the AST. For each specific checked I need to 
override the relevant visit method and collect the violation. In this way we avoid code duplications and each checked
class need to implement only the relevant logic for that specific check.

I also have a report component that groups the violation of each check in bugs, vulnerabilities and code smells.

There is also a manager component where the list of checkers and report builder are injected and provides the report
for all the files to be analyzed.

In this way we can implement any kind of required checks that can be configured in the manager component. We can also
have a different implementation of the report builder inserted into the manager. All these components are scalable.

## Limitations and Improvements
- One possible improvement is the error and exception handling.
- Enrich the junits tests to cover all possible scenarios and edge cases for checkers. 
- TBD...

## Prerequisites
* Java 1.8 or later
* Maven

## Build
```shell
mvn clean package
```

## Run
```shell
java -jar target/static-code-analyzer-1.0-SNAPSHOT.jar <list of files to be analyzed>
```

## Run unit tests
```shell
mvn verify
```