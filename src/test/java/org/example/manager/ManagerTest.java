package org.example.manager;

import org.example.checker.BaseChecker;
import org.example.data.Violation;
import org.example.data.ViolationType;
import org.example.report.Report;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ManagerTest {

    private Manager manager;

    @Mock
    private BaseChecker unusedImportsChecker;

    @Before
    public void setUp() throws Exception {
        this.manager = new Manager(Collections.singletonList(unusedImportsChecker), Report.ReportBuilder.aReport());
    }

    @Test
    public void getReport() {
        //given
        Violation bug = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.BUG)
                .withMessage(ViolationType.BUG.toString())
                .withFileName("file1")
                .withLineNumber(1)
                .build();
        Violation vul = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.VULNERABILITY)
                .withMessage(ViolationType.VULNERABILITY.toString())
                .withFileName("file2")
                .withLineNumber(1)
                .build();
        Violation codeSmell = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.CODE_SMELL)
                .withMessage(ViolationType.CODE_SMELL.toString())
                .withFileName("file3")
                .withLineNumber(1)
                .build();
        when(unusedImportsChecker.getViolations()).thenReturn(Arrays.asList(bug, vul, codeSmell));

        //when
        Map<String, List<String>> reportMap = manager.getReport(anyList());

        //then
        assertEquals(3, reportMap.size());
        assertEquals(1, reportMap.get("Bugs").size());
        assertEquals("file1:1: BUG", reportMap.get("Bugs").get(0));
        assertEquals(1, reportMap.get("Vulnerabilities").size());
        assertEquals("file2:1: VULNERABILITY", reportMap.get("Vulnerabilities").get(0));
        assertEquals(1, reportMap.get("Code Smells").size());
        assertEquals("file3:1: CODE_SMELL", reportMap.get("Code Smells").get(0));
    }
}