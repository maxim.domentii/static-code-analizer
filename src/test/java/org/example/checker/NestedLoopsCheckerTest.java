package org.example.checker;

import org.example.data.Violation;
import org.example.data.ViolationType;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NestedLoopsCheckerTest {

    private BaseChecker checker;

    @Before
    public void setUp() throws Exception {
        this.checker = new NestedLoopsChecker(3);
    }

    @Test
    public void testCheckForInvalidFilePath() {
        //given
        List<String> filePaths = new ArrayList<>(Collections.singletonList("invalid_file_path"));
        ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        //when
        checker.check(filePaths);

        //then
        assertTrue(checker.getViolations().isEmpty());
        assertEquals("File invalid_file_path not found! It will be ignored by the analyzer\n", myOut.toString());
    }

    @Test
    public void testCheckForNestedLoops() {
        //given
        String path = this.getClass().getResource("/test-files/NestedLoops.java").getPath();
        List<String> filePaths = new ArrayList<>(Collections.singletonList(path));

        //when
        checker.check(filePaths);

        //then
        assertEquals(4, checker.getViolations().size());
        for (Violation violation : checker.getViolations()) {
            assertEquals(ViolationType.CODE_SMELL, violation.getViolationType());
            assertEquals(path, violation.getFileName());
            assertEquals("too many nested for loops" , violation.getMessage());
        }
    }
}