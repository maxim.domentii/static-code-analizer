package org.example.report;

import org.example.data.Violation;
import org.example.data.ViolationType;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ReportTest {

    @Test
    public void testReportBuilder() {
        //given
        Report.ReportBuilder reportBuilder = Report.ReportBuilder.aReport();
        Violation bug = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.BUG)
                .withMessage(ViolationType.BUG.toString())
                .build();
        Violation vul = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.VULNERABILITY)
                .withMessage(ViolationType.VULNERABILITY.toString())
                .build();
        Violation codeSmell = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.CODE_SMELL)
                .withMessage(ViolationType.CODE_SMELL.toString())
                .build();

        //when
        Report report = reportBuilder.addViolations(Arrays.asList(bug, vul, codeSmell)).build();

        //then
        assertEquals(1, report.getBugs().size());
        assertEquals(ViolationType.BUG, report.getBugs().get(0).getViolationType());
        assertEquals(ViolationType.BUG.toString(), report.getBugs().get(0).getMessage());

        assertEquals(1, report.getVulnerabilities().size());
        assertEquals(ViolationType.VULNERABILITY, report.getVulnerabilities().get(0).getViolationType());
        assertEquals(ViolationType.VULNERABILITY.toString(), report.getVulnerabilities().get(0).getMessage());

        assertEquals(1, report.getCodeSmells().size());
        assertEquals(ViolationType.CODE_SMELL, report.getCodeSmells().get(0).getViolationType());
        assertEquals(ViolationType.CODE_SMELL.toString(), report.getCodeSmells().get(0).getMessage());
    }

    @Test
    public void testReportBugsForSortedViolationByFileNameAndLineNumber() {
        //given
        Report.ReportBuilder reportBuilder = Report.ReportBuilder.aReport();
        String file1 = "file1";
        Violation viol11 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.BUG)
                .withFileName(file1)
                .withLineNumber(1)
                .build();
        Violation viol12 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.BUG)
                .withFileName(file1)
                .withLineNumber(2)
                .build();
        String file2 = "file2";
        Violation viol21 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.BUG)
                .withFileName(file2)
                .withLineNumber(1)
                .build();
        Violation viol22 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.BUG)
                .withFileName(file2)
                .withLineNumber(2)
                .build();

        //when
        Report report = reportBuilder.addViolations(Arrays.asList(viol21, viol22, viol11, viol12)).build();

        //then
        List<Violation> violations = report.getBugs();
        assertEquals(4, violations.size());
        assertEquals(file1, violations.get(0).getFileName());
        assertEquals(1, violations.get(0).getLineNumber());
        assertEquals(file1, violations.get(1).getFileName());
        assertEquals(2, violations.get(1).getLineNumber());
        assertEquals(file2, violations.get(2).getFileName());
        assertEquals(1, violations.get(2).getLineNumber());
        assertEquals(file2, violations.get(3).getFileName());
        assertEquals(2, violations.get(3).getLineNumber());
    }

    @Test
    public void testReportVulForSortedViolationByFileNameAndLineNumber() {
        //given
        Report.ReportBuilder reportBuilder = Report.ReportBuilder.aReport();
        String file1 = "file1";
        Violation viol11 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.VULNERABILITY)
                .withFileName(file1)
                .withLineNumber(1)
                .build();
        Violation viol12 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.VULNERABILITY)
                .withFileName(file1)
                .withLineNumber(2)
                .build();
        String file2 = "file2";
        Violation viol21 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.VULNERABILITY)
                .withFileName(file2)
                .withLineNumber(1)
                .build();
        Violation viol22 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.VULNERABILITY)
                .withFileName(file2)
                .withLineNumber(2)
                .build();

        //when
        Report report = reportBuilder.addViolations(Arrays.asList(viol21, viol22, viol11, viol12)).build();

        //then
        List<Violation> violations = report.getVulnerabilities();
        assertEquals(4, violations.size());
        assertEquals(file1, violations.get(0).getFileName());
        assertEquals(1, violations.get(0).getLineNumber());
        assertEquals(file1, violations.get(1).getFileName());
        assertEquals(2, violations.get(1).getLineNumber());
        assertEquals(file2, violations.get(2).getFileName());
        assertEquals(1, violations.get(2).getLineNumber());
        assertEquals(file2, violations.get(3).getFileName());
        assertEquals(2, violations.get(3).getLineNumber());
    }

    @Test
    public void testReportCodeSmellsForSortedViolationByFileNameAndLineNumber() {
        //given
        Report.ReportBuilder reportBuilder = Report.ReportBuilder.aReport();
        String file1 = "file1";
        Violation viol11 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.CODE_SMELL)
                .withFileName(file1)
                .withLineNumber(1)
                .build();
        Violation viol12 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.CODE_SMELL)
                .withFileName(file1)
                .withLineNumber(2)
                .build();
        String file2 = "file2";
        Violation viol21 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.CODE_SMELL)
                .withFileName(file2)
                .withLineNumber(1)
                .build();
        Violation viol22 = Violation.ViolationBuilder.aViolation()
                .withViolationType(ViolationType.CODE_SMELL)
                .withFileName(file2)
                .withLineNumber(2)
                .build();

        //when
        Report report = reportBuilder.addViolations(Arrays.asList(viol21, viol22, viol11, viol12)).build();

        //then
        List<Violation> violations = report.getCodeSmells();
        assertEquals(4, violations.size());
        assertEquals(file1, violations.get(0).getFileName());
        assertEquals(1, violations.get(0).getLineNumber());
        assertEquals(file1, violations.get(1).getFileName());
        assertEquals(2, violations.get(1).getLineNumber());
        assertEquals(file2, violations.get(2).getFileName());
        assertEquals(1, violations.get(2).getLineNumber());
        assertEquals(file2, violations.get(3).getFileName());
        assertEquals(2, violations.get(3).getLineNumber());
    }
}