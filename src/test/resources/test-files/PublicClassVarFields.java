package org.example.checker;

import javax.annotation.Resource;
import java.util.Properties;

public class PublicClassVarFields {

    public static final String C = "constant";
    public final int n=0;

    @Resource
    public Properties properties;

    public int x;

    private int y;

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static final class InnerClass {
        public static final String C = "constant";
        public final int n=0;

        @Resource
        public Properties properties;

        public int x;

        private int y;

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }
}
