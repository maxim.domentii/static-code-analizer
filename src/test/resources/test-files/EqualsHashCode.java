package org.example.checker;

public class EqualsHashCode {

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public static final class InnerEquals {
        @Override
        public boolean equals(Object o) {
            return super.equals(o);
        }
    }

    public static final class InnerHashCode {
        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }
}

class Equals {
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}

class HashCode {
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
