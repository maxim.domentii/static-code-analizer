package org.example;

public class NestedLoops {

    public void nestedLoopsMethod() {
        for (int i=0; i<10; i++) {
            for (int j=0; j<10; j++) {
                for (int k=0; k<10; k++) {
                    System.out.println(i + ":" + j + ":" + k);
                }
            }
        }

        for (int i=0; i<10; i++) {
            for (int j=0; j<10; j++) {
                for (int k=0; k<10; k++) {
                    for (int m=0; m<10; m++) {
                        System.out.println(i + ":" + j + ":" + k);
                    }
                }
            }
        }

        for (int i : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
            for (int j : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                for (int k : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                    System.out.println(i + ":" + j + ":" + k);
                }
            }
        }

        for (int i : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
            for (int j : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                for (int k : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                    for (int m : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                        System.out.println(i + ":" + j + ":" + k);
                    }
                }
            }
        }

        for (int i=0; i<10; i++) {
            for (int j : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                for (int k=0; k<10; k++) {
                    for (int m : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                        System.out.println(i + ":" + j + ":" + k);
                    }
                }
            }
        }

        for (int i : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
            for (int j=0; j<10; j++) {
                for (int k : new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                    for (int m=0; m<10; m++) {
                        System.out.println(i + ":" + j + ":" + k);
                    }
                }
            }
        }
    }
}
