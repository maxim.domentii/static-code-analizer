package org.example.data;

public class Violation {

    private ViolationType violationType;
    private String fileName;
    private int lineNumber;
    private String message;

    public ViolationType getViolationType() {
        return violationType;
    }

    public String getFileName() {
        return fileName;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getMessage() {
        return message;
    }

    public static final class ViolationBuilder {
        private ViolationType violationType;
        private String fileName;
        private int lineNumber;
        private String message;

        private ViolationBuilder() {
        }

        public static ViolationBuilder aViolation() {
            return new ViolationBuilder();
        }

        public ViolationBuilder withViolationType(ViolationType violationType) {
            this.violationType = violationType;
            return this;
        }

        public ViolationBuilder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public ViolationBuilder withLineNumber(int lineNumber) {
            this.lineNumber = lineNumber;
            return this;
        }

        public ViolationBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Violation build() {
            Violation violation = new Violation();
            violation.violationType = this.violationType;
            violation.message = this.message;
            violation.fileName = this.fileName;
            violation.lineNumber = this.lineNumber;
            return violation;
        }
    }
}
