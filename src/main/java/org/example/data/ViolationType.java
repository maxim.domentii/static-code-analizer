package org.example.data;

public enum ViolationType {
    BUG, VULNERABILITY, CODE_SMELL;
}
