package org.example.checker;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.expr.SimpleName;
import org.example.data.Violation;
import org.example.data.ViolationType;

import java.util.*;

public class UnusedImportsChecker extends BaseChecker {

    private Map<String, Set<ImportDeclaration>> importDeclarationMap;
    private Map<String, Set<String>> nameMap;

    public UnusedImportsChecker() {
        super(ViolationType.CODE_SMELL, "Unused import {name}");
        this.importDeclarationMap = new HashMap<>();
        this.nameMap = new HashMap<>();
    }

    @Override
    public void visit(ImportDeclaration importDeclaration, List<Violation> violations) {
        super.visit(importDeclaration, violations);
        if (!importDeclarationMap.containsKey(this.filePath)) {
            importDeclarationMap.put(this.filePath, new HashSet<>());
        }
        importDeclarationMap.get(this.filePath).add(importDeclaration);
    }

    @Override
    public void visit(SimpleName name, List<Violation> violations) {
        super.visit(name, violations);
        if (!nameMap.containsKey(this.filePath)) {
            nameMap.put(this.filePath, new HashSet<>());
        }
        nameMap.get(this.filePath).add(name.asString());
    }

    @Override
    public List<Violation> getViolations() {
        this.violations = new ArrayList<>();

        for (Map.Entry<String, Set<ImportDeclaration>> entry : importDeclarationMap.entrySet()){
            String fileName = entry.getKey();
            Set<ImportDeclaration> importDeclarationSet = entry.getValue();
            for (ImportDeclaration importDeclaration : importDeclarationSet) {
                String[] importDeclTokens = importDeclaration.getNameAsString().split("\\.");
                String importDeclNameAsString = importDeclTokens[importDeclTokens.length - 1];
                if (!importDeclaration.isAsterisk() && !nameMap.get(fileName).contains(importDeclNameAsString)) {
                    this.violations.add(Violation.ViolationBuilder.aViolation()
                            .withViolationType(this.violationType)
                            .withFileName(fileName)
                            .withLineNumber(importDeclaration.getBegin().get().line)
                            .withMessage(this.message.replace("{name}",
                                    importDeclaration.getNameAsString()))
                            .build());
                }
            }
        }

        return this.violations;
    }
}
