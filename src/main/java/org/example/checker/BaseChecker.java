package org.example.checker;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.example.data.Violation;
import org.example.data.ViolationType;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseChecker extends VoidVisitorAdapter<List<Violation>> {

    protected List<Violation> violations;
    protected ViolationType violationType;
    protected String message;
    protected String filePath;

    public BaseChecker(ViolationType violationType, String message) {
        this.violations = new ArrayList<>();
        this.violationType = violationType;
        this.message = message;
    }

    public void check(List<String> filePaths) {
        for (String filePath : filePaths) {
            CompilationUnit cu = getCompilationUnit(filePath);
            if (cu != null) {
                this.filePath = filePath;
                visit(cu, violations);
            }
        }
    }

    public List<Violation> getViolations() {
        return violations;
    }

    private CompilationUnit getCompilationUnit(String  filePath) {
        try {
            return StaticJavaParser.parse(new FileInputStream(filePath));
        } catch (FileNotFoundException e) {
            System.out.println("File " + filePath + " not found! It will be ignored by the analyzer");
            return null;
        }
    }
}
