package org.example.checker;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import org.example.data.Violation;
import org.example.data.ViolationType;

import java.util.*;

public class EqualsHashCodeOverrideChecker extends BaseChecker {

    private static final String EQUALS = "equals";
    private static final String HASH_CODE = "hashCode";

    private Map<String, Map<Node, Set<MethodDeclaration>>> methodDeclarationMap;

    public EqualsHashCodeOverrideChecker() {
        super(ViolationType.BUG, "'equals(Object obj)' and 'hashCode()' should be overridden in pairs");
        this.methodDeclarationMap = new HashMap<>();
    }

    @Override
    public void visit(MethodDeclaration methodDeclaration, List<Violation> violations) {
        super.visit(methodDeclaration, violations);
        if (!methodDeclarationMap.containsKey(this.filePath)) {
            methodDeclarationMap.put(this.filePath, new HashMap<>());
        }

        Node parentClass = methodDeclaration.getParentNode().orElse(null);
        if (parentClass != null) {
            if (!methodDeclarationMap.get(this.filePath).containsKey(parentClass)) {
                methodDeclarationMap.get(this.filePath).put(parentClass, new HashSet<>());
            }
            methodDeclarationMap.get(this.filePath).get(parentClass).add(methodDeclaration);
        }
    }

    @Override
    public List<Violation> getViolations() {
        this.violations = new ArrayList<>();

        for (Map.Entry<String, Map<Node, Set<MethodDeclaration>>> entry : methodDeclarationMap.entrySet()) {
            String fileName = entry.getKey();
            Map<Node, Set<MethodDeclaration>> methodDeclarationsPerClassMap = entry.getValue();
            for (Set<MethodDeclaration> methodDeclarationsSet : methodDeclarationsPerClassMap.values()) {
                int countEqualsHashCode = 0;
                MethodDeclaration temp = null;
                for (MethodDeclaration methodDeclaration : methodDeclarationsSet) {
                    if (EQUALS.equals(methodDeclaration.getNameAsString())
                            || HASH_CODE.equals(methodDeclaration.getNameAsString())) {
                        countEqualsHashCode++;
                        temp = methodDeclaration;
                    }
                }
                if (countEqualsHashCode == 1) {
                    violations.add(Violation.ViolationBuilder.aViolation()
                                    .withViolationType(this.violationType)
                                    .withFileName(fileName)
                                    .withLineNumber(temp.getBegin().get().line)
                                    .withMessage(this.message)
                            .build());
                }
            }
        }

        return violations;
    }
}
