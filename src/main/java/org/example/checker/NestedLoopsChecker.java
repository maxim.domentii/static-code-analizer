package org.example.checker;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.stmt.ForEachStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import org.example.data.Violation;
import org.example.data.ViolationType;

import java.util.List;

public class NestedLoopsChecker extends BaseChecker {

    private int nestedLoopThreshold;
    private int loopDepth = 0;

    public NestedLoopsChecker(int nestedLoopThreshold) {
        super(ViolationType.CODE_SMELL, "too many nested for loops");
        this.nestedLoopThreshold = nestedLoopThreshold;
    }

    @Override
    public void visit(ForStmt forStmt, List<Violation> violations) {
        super.visit(forStmt, violations);
        visitFor(forStmt, true);
    }

    @Override
    public void visit(ForEachStmt forStmt, List<Violation> violations) {
        super.visit(forStmt, violations);
        visitForEach(forStmt, true);
    }

    private void visitFor(ForStmt forStmt, boolean parent) {
        if (parent) {
            this.loopDepth = 1;
        } else {
            this.loopDepth++;
        }

        for (Node child : forStmt.getBody().getChildNodes()) {
            if (child instanceof ForStmt) {
                visitFor((ForStmt) child, false);
            }
            if (child instanceof ForEachStmt) {
                visitForEach((ForEachStmt) child, false);
            }
        }

        if (parent && this.loopDepth > nestedLoopThreshold) {
            violations.add(Violation.ViolationBuilder.aViolation()
                            .withViolationType(this.violationType)
                            .withFileName(this.filePath)
                            .withLineNumber(forStmt.getBegin().get().line)
                            .withMessage(this.message)
                    .build());
        }
    }

    private void visitForEach(ForEachStmt forStmt, boolean parent) {
        if (parent) {
            this.loopDepth = 1;
        } else {
            this.loopDepth++;
        }

        for (Node child : forStmt.getBody().getChildNodes()) {
            if (child instanceof ForStmt) {
                visitFor((ForStmt) child, false);
            }
            if (child instanceof ForEachStmt) {
                visitForEach((ForEachStmt) child, false);
            }
        }

        if (parent && this.loopDepth > nestedLoopThreshold) {
            violations.add(Violation.ViolationBuilder.aViolation()
                    .withViolationType(this.violationType)
                    .withFileName(this.filePath)
                    .withLineNumber(forStmt.getBegin().get().line)
                    .withMessage(this.message)
                    .build());
        }
    }
}
