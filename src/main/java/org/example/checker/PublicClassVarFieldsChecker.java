package org.example.checker;

import com.github.javaparser.ast.body.FieldDeclaration;
import org.example.data.Violation;
import org.example.data.ViolationType;

import java.util.List;

public class PublicClassVarFieldsChecker extends BaseChecker {

    public PublicClassVarFieldsChecker() {
        super(ViolationType.VULNERABILITY,
                "Make property a static final constant or non-public and provide accessors if needed");
    }

    @Override
    public void visit(FieldDeclaration fieldDeclaration, List<Violation> violations) {
        super.visit(fieldDeclaration, violations);
        if (fieldDeclaration.isPublic() && !fieldDeclaration.isFinal() && fieldDeclaration.getAnnotations().isEmpty()) {
            this.violations.add(Violation.ViolationBuilder.aViolation()
                            .withViolationType(this.violationType)
                            .withFileName(this.filePath)
                            .withLineNumber(fieldDeclaration.getBegin().get().line)
                            .withMessage(this.message)
                    .build());
        }
    }
}
