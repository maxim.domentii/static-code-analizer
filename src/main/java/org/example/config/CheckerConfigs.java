package org.example.config;

import java.util.Properties;

public class CheckerConfigs extends Configs {

    private int nestedLoopThreshold;

    public CheckerConfigs(Properties properties) {
        super(properties);
    }

    public int getNestedLoopThreshold() {
        return nestedLoopThreshold;
    }

    @Override
    protected void initConfigs(Properties properties) {
        int nestedLoopThreshold = Integer.parseInt(properties.getProperty("checker.nestedLoopThreshold", "3"));
        if (nestedLoopThreshold < 1) {
            throw new RuntimeException("Invalid checker.nestedLoopThreshold property. Has to be > 0");
        }
        this.nestedLoopThreshold = nestedLoopThreshold;
    }
}
