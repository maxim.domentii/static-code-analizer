package org.example.config;

import java.util.Properties;

public abstract class Configs {

    public Configs(Properties properties) {
        initConfigs(properties);
    }

    protected abstract void initConfigs(Properties properties);
}
