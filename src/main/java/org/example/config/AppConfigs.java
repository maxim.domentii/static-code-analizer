package org.example.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfigs {

    public static final String APPLICATION_PROPERTIES = "application.properties";

    private CheckerConfigs checkerConfigs;

    public AppConfigs() {
        this.checkerConfigs = new CheckerConfigs(getProperties());
    }

    public CheckerConfigs getCheckerConfigs() {
        return checkerConfigs;
    }

    private Properties getProperties() {
        InputStream propsInputStream = getClass().getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES);
        Properties properties = new Properties();
        try {
            if (propsInputStream != null) {
                properties.load(propsInputStream);
            } else {
                throw new FileNotFoundException(
                        "Properties file " + APPLICATION_PROPERTIES + "not found in the classpath");
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to load application configurations", e);
        }
        return properties;
    }
}
