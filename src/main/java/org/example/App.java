package org.example;

import org.example.checker.EqualsHashCodeOverrideChecker;
import org.example.checker.NestedLoopsChecker;
import org.example.checker.PublicClassVarFieldsChecker;
import org.example.checker.UnusedImportsChecker;
import org.example.config.AppConfigs;
import org.example.manager.Manager;
import org.example.report.Report;

import java.util.Arrays;

public class App {
    public static void main( String[] args ) {
        AppConfigs appConfigs = new AppConfigs();

        Manager manager = new Manager(
                Arrays.asList(
                        new UnusedImportsChecker(),
                        new NestedLoopsChecker(appConfigs.getCheckerConfigs().getNestedLoopThreshold()),
                        new EqualsHashCodeOverrideChecker(),
                        new PublicClassVarFieldsChecker()
                ),
                Report.ReportBuilder.aReport()
        );

        System.out.println(manager.getReport(Arrays.asList(args)));
    }
}
