package org.example.manager;

import org.example.checker.BaseChecker;
import org.example.data.Violation;
import org.example.report.Report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Manager {

    private List<BaseChecker> baseCheckerList;
    private Report.ReportBuilder reportBuilder;

    public Manager(List<BaseChecker> baseCheckerList, Report.ReportBuilder reportBuilder) {
        this.baseCheckerList = baseCheckerList;
        this.reportBuilder = reportBuilder;
    }

    public Map<String, List<String>> getReport(List<String> filePaths) {
        for (BaseChecker checker : baseCheckerList) {
            checker.check(filePaths);
            reportBuilder.addViolations(checker.getViolations());
        }

        Report report = reportBuilder.build();
        List<String> bugs = new ArrayList<>();
        for (Violation violation : report.getBugs()) {
            bugs.add(violation.getFileName() + ":" + violation.getLineNumber() + ": " + violation.getMessage());
        }
        List<String> vul = new ArrayList<>();
        for (Violation violation : report.getVulnerabilities()) {
            vul.add(violation.getFileName() + ":" + violation.getLineNumber() + ": " + violation.getMessage());
        }
        List<String> codeSmells = new ArrayList<>();
        for (Violation violation : report.getCodeSmells()) {
            codeSmells.add(violation.getFileName() + ":" + violation.getLineNumber() + ": " + violation.getMessage());
        }

        Map<String, List<String>> reportMap = new HashMap<>();
        reportMap.put("Bugs", bugs);
        reportMap.put("Vulnerabilities", vul);
        reportMap.put("Code Smells", codeSmells);
        return reportMap;
    }
}
