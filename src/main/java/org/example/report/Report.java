package org.example.report;

import org.example.data.Violation;
import org.example.data.ViolationType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Report {

    private List<Violation> bugs;
    private List<Violation> vulnerabilities;
    private List<Violation> codeSmells;

    public Report() {
        this.bugs = new ArrayList<>();
        this.vulnerabilities = new ArrayList<>();
        this.codeSmells = new ArrayList<>();
    }

    public List<Violation> getBugs() {
        return bugs;
    }

    public List<Violation> getVulnerabilities() {
        return vulnerabilities;
    }

    public List<Violation> getCodeSmells() {
        return codeSmells;
    }

    public static final class ReportBuilder {
        private List<Violation> bugs = new ArrayList<>();
        private List<Violation> vulnerabilities = new ArrayList<>();
        private List<Violation> codeSmells = new ArrayList<>();

        private ReportBuilder() {
        }

        public static ReportBuilder aReport() {
            return new ReportBuilder();
        }

        public ReportBuilder addViolations(List<Violation> violations) {
            this.bugs.addAll(violations.stream()
                    .filter(v -> ViolationType.BUG.equals(v.getViolationType()))
                    .collect(Collectors.toList()));
            this.vulnerabilities.addAll(violations.stream()
                    .filter(v -> ViolationType.VULNERABILITY.equals(v.getViolationType()))
                    .collect(Collectors.toList()));
            this.codeSmells.addAll(violations.stream()
                    .filter(v -> ViolationType.CODE_SMELL.equals(v.getViolationType()))
                    .collect(Collectors.toList()));
            return this;
        }

        public Report build() {
            Report report = new Report();
            report.bugs = this.bugs.stream().sorted(getViolationComparator()).collect(Collectors.toList());
            report.vulnerabilities = this.vulnerabilities.stream().sorted(getViolationComparator())
                    .collect(Collectors.toList());
            report.codeSmells = this.codeSmells.stream().sorted(getViolationComparator()).collect(Collectors.toList());
            return report;
        }
    }

    private static Comparator<Violation> getViolationComparator() {
        return (a, b) -> a.getFileName().equals(b.getFileName()) ? a.getLineNumber() - b.getLineNumber()
                : a.getFileName().compareTo(b.getFileName());
    }
}
